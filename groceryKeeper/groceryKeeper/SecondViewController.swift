//
//  SecondViewController.swift
//  groceryKeeper
//
//  Created by Annika Wiesinger on 5/10/17.
//  Copyright © 2017 Annika Wiesinger. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, UITextFieldDelegate {
    var groceryList = [String]()
    var doubleList = [String]()
    
    @IBOutlet weak var myGroceryTableView: UITableView!
    @IBOutlet weak var newGroceryItemField: UITextField!
    
    
    @IBAction func addToList(_ sender: Any) {
        if(newGroceryItemField.text != ""){
            groceryList.append(newGroceryItemField.text!)
            doubleList.append(newGroceryItemField.text!)
            newGroceryItemField.text = ""
            myGroceryTableView.reloadData()
            newGroceryItemField.resignFirstResponder()
        }
    }
    
    @IBAction func makeList(_ sender: Any) {
        for cell in myGroceryTableView.visibleCells{
            if(cell.backgroundColor == .red){
                cell.removeFromSuperview()
            }
        }
        for item in doubleList{
            groceryList.remove(at: groceryList.index(of: item)!)
            doubleList.remove(at: doubleList.index(of: item)!)
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        if(fridgeList.contains(groceryList[indexPath.row])){
            cell.backgroundColor = .red
            cell.alpha = 0.6
        }
        cell.textLabel?.text = groceryList[indexPath.row]
        
        return (cell)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return (groceryList.count)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if(editingStyle == UITableViewCellEditingStyle.delete)
        {
            groceryList.remove(at: indexPath.row)
            myGroceryTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newGroceryItemField.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

