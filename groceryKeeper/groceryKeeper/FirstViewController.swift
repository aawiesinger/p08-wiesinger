//
//  FirstViewController.swift
//  groceryKeeper
//
//  Created by Annika Wiesinger on 5/10/17.
//  Copyright © 2017 Annika Wiesinger. All rights reserved.
//

import UIKit

var fridgeList = [String]()

class FirstViewController: UIViewController, UITabBarDelegate, UITableViewDataSource {

    @IBOutlet weak var newItem: UITextField!
    @IBOutlet weak var myFridgeTableView: UITableView!
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return (fridgeList.count)
    }
    
    @IBAction func addToList(_ sender: Any) {
        if(newItem.text != ""){
            fridgeList.append(newItem.text!)
            newItem.text = ""
            myFridgeTableView.reloadData()
            newItem.resignFirstResponder()
        }
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = fridgeList[indexPath.row]
        
        return (cell)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if(editingStyle == UITableViewCellEditingStyle.delete)
        {
                fridgeList.remove(at: indexPath.row)
                myFridgeTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

