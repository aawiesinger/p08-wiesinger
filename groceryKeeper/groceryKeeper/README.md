Grocery Keeper

The idea for this app simply came from me having lots of food in my fridge and writing lists to figure out what I had, and what I needed for the recipes I wanted to make.

To use it, the user should add to the list of items they have in the fridge/pantry/kitchen when they start using the app. Then, when they go to make a grocery list they can add everything they need for what they are planning to make, and the list will cross-reference with the contents of the fridge. 

After making their whole grocery list, the user can click "make list" to clean up the items required with what is already in the fridge. The user is left with a list of items that is not in the fridge. 

Then, while shopping the user can check off items that have been bought or added to the fridge by selecting all the items and clicking "add to fridge." This helps maintain the contents of the fridge so the user doesn't have to add them twice.

So far this app is really simple and the ux is subpar, but I have some ideas to make it an actual app going forward.
